"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""

import json
import os

from flask import Blueprint
from flask import current_app
from flask import jsonify
from flask import request
from flask import send_from_directory

from .mock_queues import MockApiReferenceTable
from .mock_queues import MockQueues
from .mock_queues import RST_MOCK

v1 = Blueprint('v1', __name__)
mockLifoQueue = MockQueues()


def createResponse(response_data: dict):
    response = current_app.response_class(response=json.dumps(response_data),
                                          status=json.dumps(response_data['status']),
                                          mimetype='application/json')

    return response


def get_data_dir():
    return os.path.join(os.path.dirname(current_app.instance_path), 'app', 'v1', 'data')


@v1.route('/', methods=['GET', 'OPTION'], strict_slashes=False)
def index():
    return jsonify({'message': os.path.dirname(current_app.instance_path)})


@v1.route('/health', methods=['GET', 'OPTION'])
def health():
    if mockLifoQueue.getRequestQueueSize(MockApiReferenceTable.GET_HEALTH) > 0 :
        return mockLifoQueue.getRequestFromQueue(MockApiReferenceTable.GET_HEALTH)
    else:
        return send_from_directory(get_data_dir(), 'health.json')


@v1.route('/books', methods=['GET', 'OPTION'])
def books():
    if mockLifoQueue.getRequestQueueSize(MockApiReferenceTable.GET_BOOKS) > 0 :
        return mockLifoQueue.getRequestFromQueue(MockApiReferenceTable.GET_BOOKS)
    else:
        return send_from_directory(get_data_dir(), 'books.json')


@v1.route('/books', methods=['POST'])
def create():
    if mockLifoQueue.getRequestQueueSize(MockApiReferenceTable.POST_BOOKS) > 0 :
        return mockLifoQueue.getRequestFromQueue(MockApiReferenceTable.POST_BOOKS)
    else:
        content = request.json
        with open(os.path.join(get_data_dir(), 'created.json')) as file:
            data = json.load(file)
            if 'title' in content.keys() and content['title'] in data.keys():
                response = createResponse(data[content['title']])
            else:
                response = createResponse(data['notOK'])

    return response

@v1.route('/books/<int:book_id>', methods=['GET', 'OPTION'])
def get_book(book_id):
    if mockLifoQueue.getRequestQueueSize(MockApiReferenceTable.GET_BOOKS_ID) > 0 :
        return mockLifoQueue.getRequestFromQueue(MockApiReferenceTable.GET_BOOKS_ID)
    else:
        return send_from_directory(get_data_dir(), f"book_{book_id}.json")

@v1.route('/books/<int:book_id>', methods=['PUT'])
def update_book(book_id):
    if mockLifoQueue.getRequestQueueSize(MockApiReferenceTable.PUT_BOOKS_ID) > 0 :
        return mockLifoQueue.getRequestFromQueue(MockApiReferenceTable.PUT_BOOKS_ID)
    else:
        with open(os.path.join(get_data_dir(), 'update.json')) as file:
            data = json.load(file)
            if book_id in data.keys():
                response = createResponse(data[book_id])
            else:
                response = createResponse(data['notOK'])
        return response


@v1.route('/books/<int:book_id>', methods=['DELETE'])
def delete_book(book_id):
    if mockLifoQueue.getRequestQueueSize(MockApiReferenceTable.DELETE_BOOKS_ID) > 0 :
        return mockLifoQueue.getRequestFromQueue(MockApiReferenceTable.DELETE_BOOKS_ID)
    else:
        return send_from_directory(get_data_dir(), 'delete.json')


@v1.route('/mock-queue', methods=['POST'])
def addToQueue():
    try:
        data = json.loads(request.data)
        if MockApiReferenceTable.has_value(data['requestMethod']):
            mockLifoQueue.remoteMockLifoQueue[data['requestMethod']].put(createResponse(data['data']))
            response = current_app.response_class(response=json.dumps({"message":"added"}),
                                                  status=json.dumps(200),
                                                  mimetype='application/json')
        elif data['requestMethod'] == RST_MOCK:
            mockLifoQueue.resetMockQueues()
            response = current_app.response_class(response=json.dumps({"message":"mocks queues are cleaned"}),
                                                  status=json.dumps(200),
                                                  mimetype='application/json')
        else:
            response = current_app.response_class(response=json.dumps({"message":"Bad Request"}),
                                                  status=json.dumps(400),
                                                  mimetype='application/json')
    except:
        current_app.logger.warning("Mock Request is incorrect: %s", json.loads(request.data))
        response = current_app.response_class(response=json.dumps({"message":"Incorrect input data format"}),
                                              status=json.dumps(400),
                                              mimetype='application/json')

    return response
