"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""

import queue

from enum import Enum

RST_MOCK = 'RST_MOCK'


class MockApiReferenceTable(str, Enum):
    """
    A class used to represent an API endpoint in the string names

    Methods
    ----------
    has_value(str|enum)
        Confirm if passed string enum name is the part of the enum class
    """
    GET_HEALTH = "get_health"
    GET_BOOKS = "get_books"
    POST_BOOKS = "post_books"
    GET_BOOKS_ID = "get_books_id"
    PUT_BOOKS_ID = "put_books_id"
    DELETE_BOOKS_ID = "delete_books_id"

    @classmethod
    def has_value(cls, key):
        """
        Confirm if passed string enum name is the part of the enum class
        """
        return key in cls._value2member_map_


class MockQueues():
    remoteMockLifoQueue = {}

    def __init__(self):
        for element in MockApiReferenceTable:
            self.remoteMockLifoQueue[element] = queue.Queue()

    def resetMockQueues(self):
        for key in self.remoteMockLifoQueue.keys():
            with self.remoteMockLifoQueue[key].mutex:
                self.remoteMockLifoQueue[key].queue.clear()
    
    def getRequestFromQueue(self, key):
        """
        Get request from Queue defined by key 
        """
        return self.remoteMockLifoQueue[key].get()

    def getRequestQueueSize(self, key) -> int:
        """
        Get amount of request stored in queue
        """
        return self.remoteMockLifoQueue[key].qsize()
